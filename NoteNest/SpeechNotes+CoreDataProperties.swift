//
//  SpeechNotes+CoreDataProperties.swift
//  NoteNest
//
//  Created by Nick Joliya on 24/09/23.
//
//

import Foundation
import CoreData


extension SpeechNotes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SpeechNotes> {
        return NSFetchRequest<SpeechNotes>(entityName: "SpeechNotes")
    }

    @NSManaged public var desc: String?
    @NSManaged public var id: UUID?
    @NSManaged public var time: Date?
    @NSManaged public var title: String?

}

//
//  AddSimpleNoteVC.swift
//  MyMemorNote
//
//  Created by Apple on 29/08/23.
//

import UIKit

@available(iOS 13.0, *)
class AddSimpleNoteVC: UIViewController {

    
    @IBOutlet weak var fieldTitle: UITextField!
    
    @IBOutlet weak var fieldDescription: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

    @IBAction func btnAdd(_ sender: UIButton) {
        
        guard let title = fieldTitle.text, title != "" else{
            return
        }
        
        guard let Description = fieldDescription.text, Description != "" else{
            return
        }
        
        DBManager().AddNoteEas(title: title, description: Description) { str in
            showAlertWithInput(str: str)
        }
        
        func showAlertWithInput(str: String){
            let alertController = UIAlertController(title: str, message: "", preferredStyle: .alert)
            let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
            })
            alertController.addAction(saveAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    

}

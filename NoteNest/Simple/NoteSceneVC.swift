//
//  NoteSceneVC.swift
//  MyMemorNote
//
//  Created by Apple on 31/08/23.
//

import UIKit

class NoteSceneVC: UIViewController {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    
    var Note: EasNotes? {
        didSet{
            print(Note as Any)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Note == nil{
            navigationController?.popViewController(animated: true)
        }
        
        lblTitle.text = Note?.title
        lblDescription.text = Note?.desc
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        lblTime.text = dateFormatter.string(from: Note?.time ?? Date())
        
    }
    
    
    
    
}

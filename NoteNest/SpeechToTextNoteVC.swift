//
//  SpeechToTextNoteVC.swift
//  NoteNest
//
//  Created by Nick Joliya on 24/09/23.
//

import UIKit

@available(iOS 13.0, *)
class SpeechToTextNoteVC: UIViewController {
    
    @IBOutlet weak var tblList: UITableView!
    
    var arrList : [SpeechNotes] = []{
        didSet{
            tblList.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblList.delegate = self
        tblList.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        arrList = DBManager().GetAllSpeech()
    }

    func showAlertWithInput(str: String){
        let alertController = UIAlertController(title: str, message: "", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
        })
        alertController.addAction(saveAction)
        present(alertController, animated: true, completion: nil)
    }
}

@available(iOS 13.0, *)
extension SpeechToTextNoteVC: UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrList.count != 0) ? arrList.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tblList.dequeueReusableCell(withIdentifier: "SpeechListCell")as! SpeechListCell
        
        if arrList.count == 0{
            
            cell = tblList.dequeueReusableCell(withIdentifier: "SpeechListCell")as! SpeechListCell
            cell.lbl.text = "No Data Found, Add Note First."
            return cell
            
        }else{
            if indexPath.row%2 != 0{
                
                cell = tblList.dequeueReusableCell(withIdentifier: "SpeechListCell")as! SpeechListCell
                cell.lbl.text = arrList[indexPath.row].title
                
            }else{
                
                cell = tblList.dequeueReusableCell(withIdentifier: "SpeechListCell")as! SpeechListCell
                cell.lbl.text = arrList[indexPath.row].title
            }
        }
        
       
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrList.count != 0{
//            print(" index:- ",indexPath.row)
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "NoteSceneVC")as! MyRecordedNotesVC
            vc.Note = arrList[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completionHandler) in
//            completionHandler(true)
//        }
//
//        let shareAction = UIContextualAction(style: .normal, title: "Share") { (action, view, completionHandler) in
//            completionHandler(true)
//        }
//
////        deleteAction.backgroundColor = .red
//        deleteAction.image = UIImage(systemName: "trash.fill")
//
////        shareAction.backgroundColor = .blue
//        shareAction.image = UIImage(systemName: "square.and.arrow.up.fill")
//
//        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, shareAction])
//        return configuration
//
//    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "New Delete") { (action, indexPath) in
            
            let alertController = UIAlertController(title: "Are You Sure To Delete This Note.", message: "", preferredStyle: .alert)
            let ActOk = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
                DBManager().DeleteNoteSpeech(id: ((self.arrList.count != 0) ? self.arrList[indexPath.row].id : nil)!) { str in
                    self.showAlertWithInput(str: str)
                    self.arrList = DBManager().GetAllSpeech()
                }
            })
            let ActCancel = UIAlertAction(title: "CANCEL", style: .default, handler: { alert -> Void in
            })
            alertController.addAction(ActOk)
            alertController.addAction(ActCancel)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        deleteAction.backgroundEffect = UIBlurEffect(style: .extraLight)
        
        deleteAction.backgroundColor = .clear
        
        return [deleteAction]
    }
    
    
}


class SpeechListCell: UITableViewCell{
    
    @IBOutlet weak var lbl: UILabel!
    
    
}

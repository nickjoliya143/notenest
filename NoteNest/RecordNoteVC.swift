//
//  RecordNoteVC.swift
//  NoteNest
//
//  Created by Nick Joliya on 24/09/23.
//

import UIKit
import Speech
@available(iOS 13.0, *)
class RecordNoteVC: UIViewController {

    
    @IBOutlet weak var fieldTitle: UITextField!
    
    @IBOutlet weak var fieldDescription: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestSpeechAuthorization()
        
    }
    @IBAction func btnSpeakDescription(_ sender: UIButton) {
        startSpeechRecognition()
    }
    @IBAction func btnAdd(_ sender: UIButton) {
        
        guard let title = fieldTitle.text, title != "" else{
            return
        }
        
        guard let Description = fieldDescription.text, Description != "" else{
            return
        }
        
        DBManager().AddNoteSpeech(title: title, description: Description) { str in
            showAlertWithInput(str: str)
        }
        
        func showAlertWithInput(str: String){
            let alertController = UIAlertController(title: str, message: "", preferredStyle: .alert)
            let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
            })
            alertController.addAction(saveAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func requestSpeechAuthorization() {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            switch authStatus {
            case .authorized:
                print("Authorization granted")
            case .denied:
                print("Authorization denied")
            case .restricted:
                print("Speech recognition is restricted on this device")
            case .notDetermined:
                print("Authorization not determined")
            @unknown default:
                fatalError("Unknown authorization status")
            }
        }
    }

        func startSpeechRecognition(){
            // Create a speech recognizer object
            let recognizer = SFSpeechRecognizer()

            // Request authorization if not already granted
            if SFSpeechRecognizer.authorizationStatus() != .authorized {
                requestSpeechAuthorization()
            }

            // Check if recognition is available
            if let recognizer = recognizer {
                let audioEngine = AVAudioEngine()
                let request = SFSpeechAudioBufferRecognitionRequest()

                do {
                    let audioSession = AVAudioSession.sharedInstance()
                    try audioSession.setCategory(.record, mode: .measurement, options: .duckOthers)
                    try audioSession.setActive(true, options: .notifyOthersOnDeactivation)

                    let inputNode = audioEngine.inputNode

                    // Use a common sample rate such as 44100 Hz (44.1 kHz)
                    let sampleRate = 44100.0

                    // Create an audio format with the specified sample rate
                    let recordingFormat = AVAudioFormat(commonFormat: .pcmFormatInt16,
                                                        sampleRate: sampleRate,
                                                        channels: 1, // Mono audio
                                                        interleaved: true)

                    // Check if the format is non-nil
                    guard let format = recordingFormat else {
                        print("Audio format is nil.")
                        return
                    }

                    inputNode.installTap(onBus: 0, bufferSize: 1024, format: format) { buffer, _ in
                        request.append(buffer)
                    }

                    audioEngine.prepare()

                    try audioEngine.start()
                    try recognizer.recognitionTask(with: request) { result, error in
                        if let result = result {
                            let recognizedText = result.bestTranscription.formattedString
                            print("Recognized Text: \(recognizedText)")
                            self.fieldDescription.text = recognizedText
                        } else if let error = error {
                            print("Recognition error: \(error)")
                        }
                    }
                } catch {
                    print("Error setting up audio session or recognition: \(error)")
                }
            } else {
                print("Speech recognition is not available on this device.")
            }
        }
}

//
//  EasNotes+CoreDataProperties.swift
//  NoteNest
//
//  Created by Nick Joliya on 24/09/23.
//
//

import Foundation
import CoreData


extension EasNotes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<EasNotes> {
        return NSFetchRequest<EasNotes>(entityName: "EasNotes")
    }

    @NSManaged public var desc: String?
    @NSManaged public var id: UUID?
    @NSManaged public var time: Date?
    @NSManaged public var title: String?

}

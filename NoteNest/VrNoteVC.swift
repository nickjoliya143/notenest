//
//  VrNoteVC.swift
//  MyMemorNote
//
//  Created by Apple on 31/08/23.
//

import UIKit
import ARKit
import Photos

class VrNoteVC: UIViewController, ARSCNViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    @IBOutlet weak var viewClick: UIView!
    
    @IBOutlet var viewNote: UIView!
    
    @IBOutlet weak var lblNote: UILabel!
    
    let configuration = ARWorldTrackingConfiguration()
    
    var selectedNode: SCNNode?
    
    var captureSession: AVCaptureSession?
    
    var movieOutput: AVCaptureMovieFileOutput?
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var strNote: String = "Demo Note"{
        didSet{
            print(strNote)
        }
    }
    
    //image delegate to share  img
    weak var delegate: ImageSelectionDelegate?
    
    //MARK: did load
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupAR()
        
        lblNote.text = strNote
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        
        sceneView.addGestureRecognizer(panGesture)
        
        addNoteInVr()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        sceneView.session.pause()
        
    }
    
    //MARK: btm snap click
    
    @IBAction func btnClick(_ sender: UIButton) {
        
        let capturedImage = sceneView.snapshot()
        
        UIImageWriteToSavedPhotosAlbum(capturedImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
        delegate?.didSelectImage(capturedImage)
        
        navigationController?.popViewController(animated: true)
        
    }
    
    func setupAR() {
        sceneView.delegate = self
        sceneView.session.run(configuration)
    }
    
   
    
    
    //MARK: Zoom In-Out Note
    
    @objc func handlePinch(_ gesture: UIPinchGestureRecognizer) {
           guard let selectedNode = selectedNode else { return }
           
           if gesture.state == .changed {
               let pinchScaleX: CGFloat = gesture.scale * CGFloat((selectedNode.scale.x))
               let pinchScaleY: CGFloat = gesture.scale * CGFloat((selectedNode.scale.y))
               let pinchScaleZ: CGFloat = gesture.scale * CGFloat((selectedNode.scale.z))
               selectedNode.scale = SCNVector3(pinchScaleX, pinchScaleY, pinchScaleZ)
               gesture.scale = 1.0
           }
       }
    
    
    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {

        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        let planeGeometry = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
        let planeNode = SCNNode(geometry: planeGeometry)
        
        planeNode.position = SCNVector3(planeAnchor.center.x, 0, planeAnchor.center.z)
        
        planeNode.eulerAngles.x = -.pi / 2
        
        node.addChildNode(planeNode)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        for childNode in node.childNodes {
            if let planeGeometry = childNode.geometry as? SCNPlane {
                planeGeometry.width = CGFloat(planeAnchor.extent.x)
                planeGeometry.height = CGFloat(planeAnchor.extent.z)
                childNode.position = SCNVector3(planeAnchor.center.x, 0, planeAnchor.center.z)
            }
        }
    }
    
    
    func addNoteInVr() {
        
        let planeGeometry = SCNPlane(width: viewNote.bounds.width / 500.0, height: viewNote.bounds.height / 500.0)
        planeGeometry.firstMaterial?.diffuse.contents = viewNote
        
        let planeNode = SCNNode(geometry: planeGeometry)
        
        planeNode.eulerAngles.x = -.pi / 2
        
        sceneView.scene.rootNode.addChildNode(planeNode)
        
        if let currentFrame = sceneView.session.currentFrame {
            var translation = matrix_identity_float4x4
            translation.columns.3.z = -0.5
            planeNode.simdTransform = simd_mul(currentFrame.camera.transform, translation)
        }
        
        selectedNode = planeNode
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch(_:)))
        sceneView.addGestureRecognizer(pinchGesture)
    }
    
    //MARK: save image
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
           if let error = error {
               print("Error saving image: \(error.localizedDescription)")
               showAlertWithInput(str: "Error saving image: \(error.localizedDescription)")
           } else {
               print("Image saved successfully.")
               showAlertWithInput(str: "Image saved successfully.")
           }
       }
    
    func showAlertWithInput(str: String){
        let alertController = UIAlertController(title: str, message: "", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
        })
        alertController.addAction(saveAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    //MARK: Handle pan gesture
    
    @objc func handlePan(_ gesture: UIPanGestureRecognizer) {
        guard let selectedNode = selectedNode else {
            return
        }

        let translation = gesture.translation(in: sceneView)

        let projectedOrigin = sceneView.projectPoint(selectedNode.position)
        let locationInWorld = sceneView.unprojectPoint(SCNVector3(x: Float(projectedOrigin.x + Float(translation.x)), y: Float(projectedOrigin.y + Float(translation.y)), z: projectedOrigin.z))

        selectedNode.position = locationInWorld

        gesture.setTranslation(.zero, in: sceneView)
    }
    
    
}

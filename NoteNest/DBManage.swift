//
//  DBManage.swift
//  MyMemorNote
//
//  Created by Apple on 29/08/23.
//

import Foundation
import UIKit
import CoreData

@available(iOS 13.0, *)

class DBManager {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    
    
    //MARK: Get Full List
    
    
    func GetAllSpeech() -> [SpeechNotes] {
        
        let arrList = [SpeechNotes]()
        
        do{
            let catchdata = try context.fetch(SpeechNotes.fetchRequest())as! [SpeechNotes]
            print("Data fetched successfully")
            for i in catchdata{
                print(i.id as Any)
                print(i.title as Any)
                print(i.desc as Any)
                print(i.time as Any)
            }
            
            return catchdata
        }catch{
            print(error.localizedDescription)
        }
        
        return arrList
    }
    
    func GetAllEasy() -> [EasNotes] {
        
        let arrList = [EasNotes]()
        
        do{
            let catchdata = try context.fetch(EasNotes.fetchRequest())as! [EasNotes]
            print("Data fetched successfully")
            for i in catchdata{
                print(i.id as Any)
                print(i.title as Any)
                print(i.desc as Any)
                print(i.time as Any)
            }
            
            return catchdata
        }catch{
            print(error.localizedDescription)
        }
        
        return arrList
    }
    
    func GetAllMid() -> [MidNotes] {
        
        let arrList = [MidNotes]()
        
        do{
            let catchdata = try context.fetch(MidNotes.fetchRequest())as! [MidNotes]
            print("Data fetched successfully")
            for i in catchdata{
                print(i.id as Any)
                print(i.title as Any)
                print(i.desc as Any)
                print(i.time as Any)
            }
            
            return catchdata
        }catch{
            print(error.localizedDescription)
        }
        
        return arrList
    }
    
    func GetAllAdv() -> [AdvNotes] {
        
        let arrList = [AdvNotes]()
        
        do{
            let catchdata = try context.fetch(AdvNotes.fetchRequest())as! [AdvNotes]
            print("Data fetched successfully")
            for i in catchdata{
                print(i.id as Any)
                print(i.title as Any)
                print(i.desc as Any)
                print(i.time as Any)
            }
            
            return catchdata
        }catch{
            print(error.localizedDescription)
        }
        
        return arrList
    }
    
    
    
    //MARK: Add Note

    func AddNoteSpeech(title:String, description: String, completion: @escaping (String) -> Void) {
        
        let entity = NSEntityDescription.entity(forEntityName: "SpeechNotes", in: context)
        let newItem = NSManagedObject(entity: entity!, insertInto: context)
        
        newItem.setValue(title, forKey: "title")
        newItem.setValue(Date(), forKey: "time")
        newItem.setValue(description, forKey: "desc")
        newItem.setValue(UUID(), forKey: "id")
        
        do{
            try context.save()
            completion("Note Added Successfully.")
        }catch{
            completion("Note Add Fail.")
        }
    }
    func AddNoteEas(title:String, description: String, completion: @escaping (String) -> Void) {
        
        let entity = NSEntityDescription.entity(forEntityName: "EasNotes", in: context)
        let newItem = NSManagedObject(entity: entity!, insertInto: context)
        
        newItem.setValue(title, forKey: "title")
        newItem.setValue(Date(), forKey: "time")
        newItem.setValue(description, forKey: "desc")
        newItem.setValue(UUID(), forKey: "id")
        
        do{
            try context.save()
            completion("Note Added Successfully.")
        }catch{
            completion("Note Add Fail.")
        }
    }
    
    func AddNoteMid(title:String, description: String, image: UIImage , completion: @escaping (String) -> Void) {
        
        let entity = NSEntityDescription.entity(forEntityName: "MidNotes", in: context)
        let newItem = NSManagedObject(entity: entity!, insertInto: context)
        
        guard let img = image.pngData() else{
            completion("Image Not Valid, Try Again.")
            return
        }
        
        newItem.setValue(title, forKey: "title")
        newItem.setValue(Date(), forKey: "time")
        newItem.setValue(description, forKey: "desc")
        newItem.setValue(UUID(), forKey: "id")
        newItem.setValue(img, forKey: "img")
        
        do{
            try context.save()
            completion("Note Added Successfully.")
        }catch{
            completion("Note Add Fail.")
        }
    }
    
    func AddNoteAdv(title:String, description: String, image: UIImage , completion: @escaping (String) -> Void) {
        
        let entity = NSEntityDescription.entity(forEntityName: "AdvNotes", in: context)
        let newItem = NSManagedObject(entity: entity!, insertInto: context)
        
        guard let img = image.pngData() else{
            completion("Image Not Valid, Try Again.")
            return
        }
        
        newItem.setValue(title, forKey: "title")
        newItem.setValue(Date(), forKey: "time")
        newItem.setValue(description, forKey: "desc")
        newItem.setValue(UUID(), forKey: "id")
        newItem.setValue(img, forKey: "img")
        
        do{
            try context.save()
            completion("Note Added Successfully.")
        }catch{
            completion("Note Add Fail.")
        }
    }
    
    //MARK: Delete Note
    
    func DeleteNoteEas(id: UUID, completion: @escaping (String) -> Void) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "EasNotes")
        request.predicate = NSPredicate(format:"id = %@", "\(id)")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        
        do {
            try context.execute(deleteRequest)
            completion("Deleted.")
            print("deleted")
        } catch let error as NSError {
            completion("Delete problem, Try Later.")
            print(error.localizedDescription)
        }
        
        
        
    }
    func DeleteNoteSpeech(id: UUID, completion: @escaping (String) -> Void) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SpeechNotes")
        request.predicate = NSPredicate(format:"id = %@", "\(id)")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        
        do {
            try context.execute(deleteRequest)
            completion("Deleted.")
            print("deleted")
        } catch let error as NSError {
            completion("Delete problem, Try Later.")
            print(error.localizedDescription)
        }
        
        
        
    }
    
    func DeleteNoteMid(id: UUID, completion: @escaping (String) -> Void) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MidNotes")
        request.predicate = NSPredicate(format:"id = %@", "\(id)")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        
        do {
            try context.execute(deleteRequest)
            completion("Deleted.")
            print("deleted")
        } catch let error as NSError {
            completion("Delete problem, Try Later.")
            print(error.localizedDescription)
        }
        
        
        
    }
    
    func DeleteNoteAdv(id: UUID, completion: @escaping (String) -> Void) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "AdvNotes")
        request.predicate = NSPredicate(format:"id = %@", "\(id)")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        
        do {
            try context.execute(deleteRequest)
            completion("Deleted.")
            print("deleted")
        } catch let error as NSError {
            completion("Delete problem, Try Later.")
            print(error.localizedDescription)
        }
        
        
        
    }
    
    
    
    
    //edit item in db
    func EditTitle(id: UUID, title: String) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TableList")
        request.predicate = NSPredicate(format:"id = %@", "\(id)")

        let result = try? context.fetch(request)

        if result?.count == 1 {
            print("data found for edit title")
            
            let object = result![0] as! NSManagedObject
            object.setValue(title, forKey: "title")
            do{
                try context.save()
            }catch{
                //error handler
            }
        }else{
            print("error")
        }
        
        
    }
    

    
    
    func changeStatus(id: UUID, isDone: Bool) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TableList")
        request.predicate = NSPredicate(format:"id = %@", "\(id)")

        let result = try? context.fetch(request)

        if result?.count == 1 {
            print("data found for edit title")
            
            let object = result![0] as! NSManagedObject
            object.setValue(isDone, forKey: "isDone")
            do{
                try context.save()
            }catch{
                //error handler
            }
        }else{
            print("error")
        }
    }
}

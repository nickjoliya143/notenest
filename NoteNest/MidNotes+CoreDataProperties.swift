//
//  MidNotes+CoreDataProperties.swift
//  MyMemorNote
//
//  Created by Apple on 29/08/23.
//
//

import Foundation
import CoreData


extension MidNotes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MidNotes> {
        return NSFetchRequest<MidNotes>(entityName: "MidNotes")
    }

    @NSManaged public var desc: String?
    @NSManaged public var id: UUID?
    @NSManaged public var img: Data?
    @NSManaged public var time: Date?
    @NSManaged public var title: String?

}

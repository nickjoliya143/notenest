//
//  AdvNotes+CoreDataProperties.swift
//  MyMemorNote
//
//  Created by Apple on 29/08/23.
//
//

import Foundation
import CoreData


extension AdvNotes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AdvNotes> {
        return NSFetchRequest<AdvNotes>(entityName: "AdvNotes")
    }

    @NSManaged public var desc: String?
    @NSManaged public var id: UUID?
    @NSManaged public var img: Data?
    @NSManaged public var time: Date?
    @NSManaged public var title: String?

}

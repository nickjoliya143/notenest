//
//  MyRecordedNotesVC.swift
//  NoteNest
//
//  Created by Nick Joliya on 24/09/23.
//

import UIKit
class MyRecordedNotesVC: UIViewController {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    
    var Note: SpeechNotes? {
        didSet{
            print(Note as Any)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Note == nil{
            navigationController?.popViewController(animated: true)
        }
        
        lblTitle.text = Note?.title
        lblDescription.text = Note?.desc
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        lblTime.text = dateFormatter.string(from: Note?.time ?? Date())
        
    }
    
    
    
    
}


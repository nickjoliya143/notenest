//
//  AddAdvNoteVC.swift
//  MyMemorNote
//
//  Created by Apple on 31/08/23.
//

import UIKit



@available(iOS 13.0, *)

class AddAdvNoteVC: UIViewController , ImageSelectionDelegate {
    
    @IBOutlet weak var fieldTitle: UITextField!
    
    @IBOutlet weak var fieldNote: UITextField!
    
    @IBOutlet weak var fieldDescription: UITextView!
    
    @IBOutlet weak var imgNote: UIImageView!
    
    var imagePicker = UIImagePickerController()
    
    var img: UIImage? {
        didSet{
            imgNote.image = img
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func btnOpenCam(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "VrNoteVC")as! VrNoteVC
        vc.strNote = fieldNote.text ?? "Note Object."
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
   

    @IBAction func btnAdd(_ sender: UIButton) {
        
        guard let title = fieldTitle.text, title != "" else{
            return
        }
        
        guard let Description = fieldDescription.text, Description != "" else{
            return
        }
        
        guard img != nil else{
            return
        }
        
        DBManager().AddNoteAdv(title: title, description: Description, image: img!) { str in
            showAlertWithInput(str: str)
        }
        
        func showAlertWithInput(str: String){
            let alertController = UIAlertController(title: str, message: "", preferredStyle: .alert)
            let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
            })
            alertController.addAction(saveAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func didSelectImage(_ image: UIImage?) {
        img = image
    }

}

//
//  Image + Protocol.swift
//  MyMemorNote
//
//  Created by Apple on 31/08/23.
//

import Foundation
import UIKit

protocol ImageSelectionDelegate: class {
    
    func didSelectImage(_ image: UIImage?)
    
}

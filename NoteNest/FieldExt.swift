//
//  FieldExt.swift
//  MyMemorNote
//
//  Created by Apple on 31/08/23.
//

import Foundation
import UIKit

class FieldClass: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupDoneButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDoneButton()
    }
    
    private func setupDoneButton() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonTapped))
        
        toolbar.setItems([flexibleSpace, doneButton], animated: false)
        
        inputAccessoryView = toolbar
    }
    
    @objc private func doneButtonTapped() {
        resignFirstResponder()
    }
}

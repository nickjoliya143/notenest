//
//  AdvNotesVC.swift
//  MyMemorNote
//
//  Created by Apple on 31/08/23.
//

import UIKit

@available(iOS 13.0, *)
class AdvNotesVC: UIViewController {
    
    @IBOutlet weak var tblList: UITableView!
    
    var arrList : [AdvNotes] = []{
        didSet{
            tblList.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblList.delegate = self
        tblList.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        arrList = DBManager().GetAllAdv()
    }

    func showAlertWithInput(str: String){
        let alertController = UIAlertController(title: str, message: "", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
        })
        alertController.addAction(saveAction)
        present(alertController, animated: true, completion: nil)
    }
}


@available(iOS 13.0, *)
extension AdvNotesVC: UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrList.count != 0) ? arrList.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblList.dequeueReusableCell(withIdentifier: "AdvListCell")as! AdvListCell
        
        if arrList.count == 0{
            
            cell.lbl.text = "No Data Found, Add Note First."
            
        }else{
            
            cell.lbl.text = arrList[indexPath.row].title
            
            cell.img.image = (arrList[indexPath.row].img != nil) ? UIImage(data: (arrList[indexPath.row].img ?? UIImage(named: "icon")?.pngData()!)!) : cell.img.image
            
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrList.count != 0{
            
//            let vc = storyboard?.instantiateViewController(withIdentifier: "NoteSceneVC")as! NoteSceneVC
//            vc.Note = arrList[indexPath.row]
//            navigationController?.pushViewController(vc, animated: true)
            
        }
    }

    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "New Delete") { (action, indexPath) in
            
            let alertController = UIAlertController(title: "Are You Sure To Delete This Note.", message: "", preferredStyle: .alert)
            
            let ActOk = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
                
                if self.arrList.count == 0{
                    return
                }
                
                DBManager().DeleteNoteAdv(id: ((self.arrList.count != 0) ? self.arrList[indexPath.row].id : nil)!) { str in
                    self.showAlertWithInput(str: str)
                    self.arrList = DBManager().GetAllAdv()
                }
            })
            
            let ActCancel = UIAlertAction(title: "CANCEL", style: .default, handler: { alert -> Void in
            })
            
            alertController.addAction(ActOk)
            alertController.addAction(ActCancel)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        deleteAction.backgroundEffect = UIBlurEffect(style: .extraLight)
        
        deleteAction.backgroundColor = .clear
        
        return [deleteAction]
    }
    
    
}


class AdvListCell: UITableViewCell{
    
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    
}

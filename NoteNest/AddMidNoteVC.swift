//
//  AddMidNoteVC.swift
//  MyMemorNote
//
//  Created by Apple on 31/08/23.
//

import UIKit

@available(iOS 13.0, *)
class AddMidNoteVC: UIViewController {
    
    @IBOutlet weak var fieldTitle: UITextField!
    
    @IBOutlet weak var fieldDescription: UITextView!
    
    @IBOutlet weak var imgNote: UIImageView!
    
    var imagePicker = UIImagePickerController()
    
    var img: UIImage? {
        didSet{
            imgNote.image = img
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func btnCamera(_ sender: UIButton) {
        openCamera()
    }
    
    @IBAction func btnGallery(_ sender: UIButton) {
        openGallery()
    }

    @IBAction func btnAdd(_ sender: UIButton) {
        
        guard let title = fieldTitle.text, title != "" else{
            return
        }
        
        guard let Description = fieldDescription.text, Description != "" else{
            return
        }
        
        guard img != nil else{
            return
        }
        
        DBManager().AddNoteMid(title: title, description: Description, image: img!) { str in
            showAlertWithInput(str: str)
        }
        
        func showAlertWithInput(str: String){
            let alertController = UIAlertController(title: str, message: "", preferredStyle: .alert)
            let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
            })
            alertController.addAction(saveAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    

}

import UIKit

@available(iOS 13.0, *)
extension AddMidNoteVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }else{
            openGallery()
        }
    }
    
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.originalImage] as? UIImage {
            img = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

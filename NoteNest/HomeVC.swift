//
//  HomeVC.swift
//  MyMemorNote
//
//  Created by Apple on 29/08/23.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var img1: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LoadGetStart()
        
    }
    

    func LoadGetStart(){
        let vc = storyboard?.instantiateViewController(withIdentifier: "GetStartedVC")as! GetStartedVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
    }

}
